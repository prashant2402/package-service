import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Response } from '@angular/http';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-package-create',
  templateUrl: './package-create.component.html',
  styleUrls: ['./package-create.component.css']
})

export class PackageCreateComponent implements OnInit {
  baseUrl = environment.baseUrl;
  private apiUrl = this.baseUrl + '/packages';

  products : { value: string }[] = [];
  name : any;
  description: any;
  errors : any;

  productpackage = { products:this.products, name: this.name, description:this.description };
  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }

  savePackage() {
    var productIds = this.productpackage.products.map(a => a.value);
    this.productpackage['productIds'] = productIds;
    this.http.post(this.apiUrl, this.productpackage)
      .subscribe(res => {
          this.router.navigate(['/packages', res]);
        }, (err) => {
          this.errors = err.error.status + ':' + err.error.message;
        }
      );
  }

  addProduct() {
    this.productpackage.products.push({ value: 'Enter a valid Product Id'});
  };

  removeProduct(productToRemove) {
    var index = this.productpackage.products.indexOf(productToRemove);
    this.productpackage.products.splice(index, 1);
  };

}
