import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PackagesComponent } from './packages/packages.component';
import { PackageCreateComponent } from './package-create/package-create.component';

const routes: Routes = [

{ path: 'packages', component: PackagesComponent },
{
path: 'package-create',
component: PackageCreateComponent
},
{ path: '', redirectTo: 'packages', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
