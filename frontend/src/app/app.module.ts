import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { PackagesComponent } from './packages/packages.component';
import { PackageDetailComponent } from './package-detail/package-detail.component';
import { PackageCreateComponent } from './package-create/package-create.component';
import { PackageUpdateComponent } from './package-update/package-update.component';
import { MenuService } from './menu.service';

const appRoutes: Routes = [
{
path: 'packages',
component: PackagesComponent,
data: { title: 'Packages' }
},
{
path: 'package-detail/:id',
component: PackageDetailComponent,
data: { title: 'Package Details' }
},
{
path: 'package-create',
component: PackageCreateComponent,
data: { title: 'Create Package' }
},
{
path: 'package-update/:id',
component: PackageUpdateComponent,
data: { title: 'Update Package' }
},
{ path: '',
redirectTo: '/packages',
pathMatch: 'full'
}
];

@NgModule({
  declarations: [
    AppComponent,
    PackagesComponent,
    PackageDetailComponent,
    PackageCreateComponent,
    PackageUpdateComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [MenuService],
  bootstrap: [AppComponent]
})
export class AppModule { }
