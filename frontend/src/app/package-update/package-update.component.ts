import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-package-update',
  templateUrl: './package-update.component.html',
  styleUrls: ['./package-update.component.css']
})
export class PackageUpdateComponent implements OnInit {
  baseUrl = environment.baseUrl;
  private apiUrl = this.baseUrl + '/packages/';

  productsIds : { value: string }[] = [];
  productpackage: any;
  errors : any;

  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getPackage(this.route.snapshot.params['id']);
  }

  getPackage(id) {
    this.http.get(this.apiUrl + id).subscribe(data => {
      this.productpackage = data;
    });

  }

  updatePackage(id) {
    var productIds = this.productpackage.products.map(a => a.id);
    this.productpackage['productIds'] = productIds;
    this.http.put(this.apiUrl + id, this.productpackage)
      .subscribe(res => {
          let id = res['id'];
          this.router.navigate(['/package-detail', id], { skipLocationChange: true });
        }, (err) => {
          this.errors = err.error.status + ':' + err.error.message;
        }
      );
  }

 addProduct() {
    this.productpackage.products.push({ value: 'Enter a valid Product Id'});
  };

  removeProduct(productToRemove) {
    var index = this.productpackage.products.indexOf(productToRemove);
    this.productpackage.products.splice(index, 1);
  };

}
