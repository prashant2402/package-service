import{Component, OnInit}from '@angular/core';
import {HttpClient}from '@angular/common/http';
import {ActivatedRoute, Router}from '@angular/router';
import {Location}from '@angular/common';
import {Response}from '@angular/http';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-package-detail',
  templateUrl: './package-detail.component.html',
  styleUrls: ['./package-detail.component.css']
})
export class PackageDetailComponent implements OnInit {
  baseUrl = environment.baseUrl;
  private apiUrl = this.baseUrl + '/packages/';
  private currencyUrl = this.baseUrl + '/currency';

  productpackage : any;
  currencies: any;
  currency = 'USD';
  priceString = '';
  errors : any;

  constructor(private router: Router, private route: ActivatedRoute, private http: HttpClient, private location: Location) { }

  ngOnInit() {
    this.getPackageDetail(this.route.snapshot.params['id']);
    this.getCurrencies();
  }

  getPackageDetail(id) {
    this.http.get(this.apiUrl + id).subscribe(data => {
      this.productpackage = data;
      this.priceString = this.productpackage.price + ' ' + this.currency;
    });
  }

  getPackageDetailInCurrency(id, currency: any) {
    this.currency = currency;
    this.http.get(this.apiUrl + id + '?currency=' + currency).subscribe(data => {
      this.productpackage = data;
      this.priceString = this.productpackage.price + ' ' + this.currency;
    });
  }

  getCurrencies() {
    this.http.get(this.currencyUrl).subscribe(data => {
      this.currencies = data;
    });
  }

  deletePackage(id) {
  this.http.delete(this.apiUrl + id)
    .subscribe(res => {
         this.router.navigate(['/packages']);
      }, (err) => {
        this.errors = err.error.status + ':' + err.error.message;
      }
    );
  }

}
