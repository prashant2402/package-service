import { Injectable } from '@angular/core';

@Injectable()
export class MenuService {

constructor() { }

  getMenu(): Array<any> {
    const menu = [
      { name: 'packages', path: './packages', children: [] },
      { name: 'package-create', path: './package-create', children: [] },
    ];

    return menu;
  }

}
