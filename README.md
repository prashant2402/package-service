# Package Service API

a RESTful web service for managing packages of products

## Endpoints  
| Method | URI       | Description       |
| ------ | --------- | ----------------- |
| GET    | /packages | List all packages | 
| POST   | /packages | Create a new package | 
| DELETE | /packages/{packageId} | Deletes a package by its ID | 
| GET    | /packages/{packageId} | Returns a single package by its ID | 
| PUT    | /packages/{packageId} | Update an existing package | 
| GET    | /currency | List all supported currencies | 

## Overview
I've followed API-First development strategy in order to implement this RESTful web service i.e,
to build the API first and build application on top of that API.

I've used Swagger OpenAPI specification to document the REST API. The API definition can be found in the project root folder:
```
./openapi.yaml
```  
Once the application is running, you can open this URL to view the Swagger UI for API documentation and also for testing the API:
[http://localhost:8080/api/v1](http://localhost:8080/api/v1)

For persistence, I've used Mongodb database (in embedded mode to keep it simple).

TDD/BDD approach is strictly followed and I've used Cucumber/SpringTest/JUnit/Mockito tools to implement Integration and Unit tests. 
There are very few cucumber scenarios (_updatePackage.feature_) that I couldn't implement due to lack of time.

### To run the service in your local environment
`Pre-requisites`: You should have JDK8 to run and Maven 2 installed in order to build and run the application.
```
$ git clone https://github.com/prashant2402/package-service.git
$ cd package-service
$ mvn spring-boot:run
```  
And then, use Swagger UI to run sample requests against the API:
[http://localhost:8080/api/v1](http://localhost:8080/api/v1)
If you are a fan of Postman or simple Curl, then still visit Swagger UI to grab sample requests :smiley: and run in the tool of your choice.

### Tests
Unit tests can be run using:
```
mvn test 
```

Integration tests can be run using (these are configured not to be run as part regular unit tests ie., surefire-plugin because in real project Integration tests can run for long time and hence, not best to run via _mvn test_):
```
mvn verify
```

## Angular Frontend
From the `frontend` folder in the root project, run these commands to build and run Angular Server and then you can browse to [http://localhost:4200](http://localhost:4200):
```
$ npm install
$ npm run-script build
$ ng serve --o
```

The frontend supports all endpoints implemented by the API.

### Production Ready Docker build
#### _with a thin and efficient Nginx image, with just the compiled code_
To build your image using the production configuration (the default):
```
$ docker build -t package-service-frontend:prod  .
```
Test your image for the production environment (production configuration) with:
```
$ docker run -p 80:80 package-service-frontend:prod
```
To build your image using the development environment (no configuration):
```
$ docker build -t package-service-frontend:dev --build-arg configuration="" .
```
Test your image for the development environment (no configuration) with:
```
$ docker run -p 80:80 package-service-frontend:dev
```
Open your browser in http://localhost.