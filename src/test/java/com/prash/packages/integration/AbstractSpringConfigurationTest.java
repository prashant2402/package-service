package com.prash.packages.integration;

import com.prash.packages.invoker.PackageServiceApplication;
import java.text.MessageFormat;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest(
    classes = PackageServiceApplication.class,
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractSpringConfigurationTest {
  protected RestTemplate restTemplate;

  @Autowired protected World world;

  @LocalServerPort protected int port;

  public AbstractSpringConfigurationTest() {
    restTemplate = new RestTemplate();
  }

  public String getPackagesUrl() {
    return MessageFormat.format("http://localhost:{0}/api/v1/packages", String.valueOf(port));
  }
}
