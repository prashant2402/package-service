package com.prash.packages.integration;

import com.prash.packages.model.PackageModel;
import com.prash.packages.model.PackageRequest;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@Scope("cucumber-glue")
public class World {

  private PackageRequest packageRequest;

  private PackageModel packageModel;

  private ResponseEntity response;

  private int errorCode;

  public PackageRequest getPackageRequest() {
    return packageRequest;
  }

  public void setPackageRequest(PackageRequest packageRequest) {
    this.packageRequest = packageRequest;
  }

  public PackageModel getPackageModel() {
    return packageModel;
  }

  public void setPackageModel(PackageModel packageModel) {
    this.packageModel = packageModel;
  }

  public ResponseEntity getResponse() {
    return response;
  }

  public void setResponse(ResponseEntity response) {
    this.response = response;
  }

  public int getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(int errorCode) {
    this.errorCode = errorCode;
  }
}
