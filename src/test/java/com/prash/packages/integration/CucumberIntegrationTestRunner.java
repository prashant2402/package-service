package com.prash.packages.integration;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = {
    "src/test/resources/features/createPackage.feature",
    "src/test/resources/features/deletePackage.feature",
    "src/test/resources/features/listPackage.feature",
    "src/test/resources/features/retrievePackage.feature"
        },
        tags = {"not @ignore"},
        glue = {"com.prash.packages.integration.steps"},
        plugin = {"pretty", "html:target/cucumber"})
public class CucumberIntegrationTestRunner {
}
