package com.prash.packages.integration.steps;

import com.prash.packages.integration.AbstractSpringConfigurationTest;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class UpdatePackageSteps extends AbstractSpringConfigurationTest {
  @When("^update package is invoked with name `(.*)`$")
  public void update_package_is_invoked_with_name(String name) throws Exception {
    throw new PendingException();
  }

  @Then(
      "^the package is updated with name `(.*)`, description `(.*)`, products `(.*)` and price `(\\d+)`$")
  public void the_package_is_updated_with_name_description_products_and_price(
      String name, String description, String products, int price) throws Exception {
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException();
  }

  @Then("^the package is updated with products `(.*)`$")
  public void the_package_is_updated_with_products(String products) throws Exception {
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException();
  }

  @When("^update package is invoked with products `(.*)`$")
  public void update_package_is_invoked_with_products(String products) throws Exception {
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException();
  }

  @Given("^there is a valid package and an invalid product `(.*)`$")
  public void there_is_a_valid_package_and_an_invalid_product(String product) throws Exception {
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException();
  }
}
