package com.prash.packages.integration.steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;

import com.prash.packages.api.repository.PackagesRepository;
import com.prash.packages.integration.AbstractSpringConfigurationTest;
import com.prash.packages.model.PackageModel;
import com.prash.packages.model.PackageRequest;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

public class ListPackageSteps extends AbstractSpringConfigurationTest {
  @Autowired
  PackagesRepository packagesRepository;

  @Given("^there are no packages$")
  public void there_are_no_packages() throws Exception {
    packagesRepository.deleteAll();
  }

  @When("^list packages is invoked$")
  public void list_packages_is_invoked() throws Exception {
    ResponseEntity<List<PackageModel>> responseEntity =
        restTemplate.exchange(getPackagesUrl(), HttpMethod.GET, null,
            new ParameterizedTypeReference<List<PackageModel>>(){});

    world.setResponse(responseEntity);
  }

  @Then("^no packages are returned$")
  public void no_packages_are_returned() throws Exception {
    List<PackageModel> packageModels = (List<PackageModel>) world.getResponse().getBody();
    assertThat(packageModels.size(), is(equalTo(0)));
  }

  @Given("^there are packages `(.*)`$")
  public void there_are_packages(String packages) throws Exception {
    Arrays.stream(packages.split("\\s*,\\s*")).forEach(name -> {
      PackageRequest packageRequest =
          new PackageRequest().name(name).description("desc").productIds(Arrays.asList("PKM5pGAh9yGm"));
      ResponseEntity<PackageModel> responseEntity =
          restTemplate.postForEntity(getPackagesUrl(), packageRequest, PackageModel.class);
      assertThat(responseEntity.getStatusCodeValue(), is(equalTo(201)));
    });

  }

  @Then("^packages `(.*)` are returned$")
  public void packages_are_returned(String packages) throws Exception {
    List<PackageModel> packageModels = (List<PackageModel>) world.getResponse().getBody();
    Arrays.stream(packages.split("\\s*,\\s*")).forEach(name ->
        assertTrue(packageModels.stream().anyMatch(packageModel -> packageModel.getName().equalsIgnoreCase(name))));
  }
}
