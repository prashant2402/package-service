package com.prash.packages.integration.steps;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.prash.packages.integration.AbstractSpringConfigurationTest;
import com.prash.packages.model.PackageModel;
import com.prash.packages.model.PackageRequest;
import com.prash.packages.model.Product;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

public class CreatePackageSteps extends AbstractSpringConfigurationTest {
  @Given("^a package with name `(.*)`, description `(.*)` and productIds `(.*)`$")
  public void given_a_package_with_name_description_and_productIds(
      String name, String description, String productIds) throws Exception {
    List<String> productList = Arrays.asList(productIds.split("\\s*,\\s*"));
    PackageRequest packageRequest =
        new PackageRequest().name(name).description(description).productIds(productList);
    world.setPackageRequest(packageRequest);
  }

  @When("^create package is invoked$")
  public void create_package_is_invoked() throws Exception {
    PackageRequest packageRequest = world.getPackageRequest();
    try {

      ResponseEntity<PackageModel> responseEntity =
          restTemplate.postForEntity(getPackagesUrl(), packageRequest, PackageModel.class);
      world.setPackageModel(responseEntity.getBody());

    } catch (HttpClientErrorException e) {
      world.setErrorCode(e.getRawStatusCode());
    }
  }

  @Then("^the package `(.*)` is created with products `(.*)` and price `(.*)`$")
  public void the_package_packageA_is_created_with_products_and_price(
      String name, String products, double price) throws Exception {
    List<Product> productsExpected = new ArrayList<>();
    JSONArray jsonArray = new JSONArray(products);
    for (int i = 0; i < jsonArray.length(); i++) {
      JSONObject jsonObj = jsonArray.getJSONObject(i);
      productsExpected.add(
          new Product()
              .id(jsonObj.getString("id"))
              .name(jsonObj.getString("name"))
              .usdPrice(Integer.valueOf(jsonObj.getString("usdPrice"))));
    }

    assertNotNull(world.getPackageModel().getId());
    assertTrue(
        new HashSet<>(world.getPackageModel().getProducts())
            .equals(new HashSet<>(productsExpected)));
    assertThat(world.getPackageModel().getPrice(), is(equalTo(price)));
  }
}
