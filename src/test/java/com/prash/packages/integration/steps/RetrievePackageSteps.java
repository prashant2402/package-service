package com.prash.packages.integration.steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.prash.packages.integration.AbstractSpringConfigurationTest;
import com.prash.packages.model.PackageModel;
import com.prash.packages.model.PackageRequest;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class RetrievePackageSteps extends AbstractSpringConfigurationTest {

  @Value("${currencyconverterapi.convert}")
  private String currencyConvertEndpoint;
  @Value("${products.api.base.currency}")
  private String productsApiBaseCurrency;
  private static final ObjectMapper MAPPER = new ObjectMapper();

  @Given("^there is a package with name `(.*)`, description `(.*)` and products `(.*)`$")
  public void there_is_a_package_with_name_description_and_products(
      String name, String description, String products) throws Exception {
    List<String> productList = new ArrayList<>();
    JSONArray jsonArray = new JSONArray(products);
    for (int i = 0; i < jsonArray.length(); i++) {
      JSONObject jsonObj = jsonArray.getJSONObject(i);
      productList.add(jsonObj.getString("id"));
    }
    PackageRequest packageRequest =
        new PackageRequest().name(name).description(description).productIds(productList);
    ResponseEntity<PackageModel> responseEntity =
        restTemplate.postForEntity(getPackagesUrl(), packageRequest, PackageModel.class);
    world.setPackageModel(responseEntity.getBody());
    assertThat(responseEntity.getStatusCodeValue(), is(equalTo(201)));
  }

  @When("^retrieve package is invoked$")
  public void retrieve_package_is_invoked() {
    ResponseEntity<PackageModel> responseEntity =
        restTemplate.getForEntity(
            getPackagesUrl() + "/" + world.getPackageModel().getId(), PackageModel.class);
    world.setResponse(responseEntity);
  }

  @When("^retrieve package is invoked with id (.*)$")
  public void retrieve_package_is_invoked_with_id(String id) {
    try {
      ResponseEntity<PackageModel> responseEntity =
          restTemplate.getForEntity(getPackagesUrl() + "/" + id, PackageModel.class);
      world.setResponse(responseEntity);
    } catch (HttpClientErrorException e) {
      world.setErrorCode(e.getRawStatusCode());
    }
  }

  @When("^retrieve package is invoked in currency `(.*)`$")
  public void retrieve_package_is_invoked_in_currency(String currency) {
    try {
      RestTemplate currencyRestTemplate = new RestTemplate();
      ResponseEntity<PackageModel> responseEntity =
          currencyRestTemplate.getForEntity(
              getPackagesUrl() + "/" + world.getPackageModel().getId() + "?currency=" + currency,
              PackageModel.class);
      world.setResponse(responseEntity);
    } catch (HttpClientErrorException e) {
      world.setErrorCode(e.getRawStatusCode());
    }
  }

  @Then("^price is in `(.*)` calculated using latest forex rate$")
  public void price_is_in_calculated_using_latest_forex_rate(String currency) throws Exception {
    double usdPrice = world.getPackageModel().getPrice();
    PackageModel packageModel = (PackageModel) world.getResponse().getBody();
    String url = MessageFormat.format(currencyConvertEndpoint, productsApiBaseCurrency, currency, packageModel.getPrice());
    ResponseEntity<String> responseEntity  = restTemplate.getForEntity(url, String.class);
    JsonNode root = MAPPER.readTree(responseEntity.getBody());
    JsonNode name = root.path(productsApiBaseCurrency + "_" + currency);
    double forexPrice = Double.valueOf(name.asText());
    forexPrice = new BigDecimal(forexPrice * usdPrice).setScale(2, RoundingMode.HALF_UP).doubleValue();
    assertThat(packageModel.getPrice(), is(forexPrice));
  }

  @Then(
      "^the package is retrieved with name `(.*)`, description `(.*)`, products `(.*)` and price `(\\d+)`$")
  public void the_package_is_retrieved_with_name_description_products_and_price(
      String name, String description, String products, double price) {
    PackageModel packageModel = (PackageModel) world.getResponse().getBody();
    assertThat(packageModel.getName(), is(equalTo(name)));
    assertThat(packageModel.getDescription(), is(equalTo(description)));
    assertThat(packageModel.getPrice(), is(equalTo(price)));
  }

  @Then("^the package is retrieved with name `(.*)`, description `(.*)`, products `(.*)`$")
  public void the_package_is_retrieved_with_name_description_products(
      String name, String description, String products) {
    PackageModel packageModel = (PackageModel) world.getResponse().getBody();
    assertThat(packageModel.getName(), is(equalTo(name)));
    assertThat(packageModel.getDescription(), is(equalTo(description)));
  }
}
