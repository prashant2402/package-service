package com.prash.packages.integration.steps;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import com.prash.packages.integration.AbstractSpringConfigurationTest;
import cucumber.api.java.en.Then;
import org.springframework.http.HttpStatus;

public class CommonSteps extends AbstractSpringConfigurationTest {

  @Then("^request fails with status code (\\d+)$")
  public void request_fails_with_status_code(int statusCode) {
    assertThat(world.getErrorCode(), is(equalTo(statusCode)));
  }

  @Then("^response status is (\\d+)$")
  public void response_status_is(int statusCode) {
    assertThat(world.getResponse().getStatusCode(), is(HttpStatus.valueOf(statusCode)));
  }
}
