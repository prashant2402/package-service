package com.prash.packages.integration.steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.fail;

import com.prash.packages.integration.AbstractSpringConfigurationTest;
import com.prash.packages.model.PackageModel;
import com.prash.packages.model.PackageRequest;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.util.Arrays;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

public class DeletePackageSteps extends AbstractSpringConfigurationTest {
  @Given("^there is a package with name `(.*)` and product id `(.*)`$")
  public void there_is_a_package_with_name(String name, String productId) {
    PackageRequest packageRequest =
        new PackageRequest().name(name).description("desc").productIds(Arrays.asList(productId));
    ResponseEntity<PackageModel> responseEntity =
        restTemplate.postForEntity(getPackagesUrl(), packageRequest, PackageModel.class);
    assertThat(responseEntity.getStatusCodeValue(), is(equalTo(201)));
    world.setPackageModel(responseEntity.getBody());
  }

  @When("^delete package is invoked$")
  public void delete_package_is_invoked() {
    restTemplate.delete(getPackagesUrl() + "/" + world.getPackageModel().getId());
  }

  @When("^delete package is invoked with id `(.*)`$")
  public void delete_package_is_invoked_withid(String id) {
    ResponseEntity<String> response =
        restTemplate.exchange(getPackagesUrl() + "/" + id, HttpMethod.DELETE, null, String.class);
    world.setResponse(response);
  }

  @Then("^the package `(.*)` is deleted$")
  public void the_package_is_deleted(String name) {
    try {
      restTemplate.getForObject(
          getPackagesUrl() + "/" + world.getPackageModel().getId(), PackageModel.class);
      fail("expected error");
    } catch (HttpClientErrorException e) {
      assertThat(e.getRawStatusCode(), is(equalTo(404)));
    }
  }

  @Given("^there is no package with package id `(.*)`$")
  public void there_is_no_package_with_package_id(String id) {
    try {
      restTemplate.getForObject(getPackagesUrl() + "/" + id, PackageModel.class);
      fail("expected error");
    } catch (HttpClientErrorException e) {
      assertThat(e.getRawStatusCode(), is(equalTo(404)));
    }
  }
}
