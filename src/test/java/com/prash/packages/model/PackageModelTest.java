package com.prash.packages.model;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class PackageModelTest {
  @Test
  public void shouldCalculateTotalPrice() {
    // given
    PackageModel aPackage =
        new PackageModel()
            .addProductsItem(new Product().usdPrice(999))
            .addProductsItem(new Product().usdPrice(499));

    // when
    Double totalPrice = aPackage.calculateTotalPrice();

    // then
    assertThat(totalPrice, is(equalTo(14.98)));
  }

  @Test
  public void shouldNotCalculateTotalPriceIfNoProducts() {
    // given
    PackageModel aPackage = new PackageModel();

    // when
    Double totalPrice = aPackage.calculateTotalPrice();

    // then
    assertNull(totalPrice);
  }

  @Test
  public void shouldCalculatePriceForOtherCurrency() {
    // given
    PackageModel aPackage = new PackageModel().price(14.98);

    // when
    Double calculatePriceForOtherCurrency = aPackage.calculatePriceForOtherCurrency(1.176602);

    // then
    assertThat(calculatePriceForOtherCurrency, is(equalTo(17.63)));
  }
}
