package com.prash.packages.api;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withBadRequest;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prash.packages.exception.BadRequestException;
import com.prash.packages.invoker.PackageServiceApplication;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PackageServiceApplication.class)
public class CurrencyConverterServiceImplTest {

  @Value("${currencyconverterapi.convert}")
  private String currencyConvertEndpoint;

  @Value("${currencyconverterapi.symbols}")
  private String currencySymbolsEndpoint;

  private MockRestServiceServer server;

  @Autowired private ObjectMapper objectMapper;

  @Autowired private CurrencyConverterServiceImpl currencyConverterService;

  @Before
  public void setUp() {
    server = MockRestServiceServer.createServer(currencyConverterService.getRestTemplate());
  }

  @Test
  public void shouldConvertFromUSDToGBP() throws Exception {
    double amount = 14.99;
    String baseCurrency = "USD";
    String targetCurrency = "GBP";
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("USD_GBP", 71.607503);
    server
        .expect(
            ExpectedCount.times(1),
            requestTo(
                MessageFormat.format(
                    currencyConvertEndpoint, baseCurrency, targetCurrency, amount)))
        .andRespond(withSuccess(jsonObject.toString(), MediaType.APPLICATION_JSON));

    double convertedAmount = currencyConverterService.convert(baseCurrency, targetCurrency, amount);

    assertThat(convertedAmount, is(equalTo(1073.39646997)));
  }

  @Test(expected = BadRequestException.class)
  public void shouldNotConvertIfInvalidBaseCurrency() {
    double amount = 14.99;
    String fromCurrency = "notvalid";
    String targetCurrency = "GBP";
    server
        .expect(
            ExpectedCount.times(1),
            requestTo(
                MessageFormat.format(
                    currencyConvertEndpoint, fromCurrency, targetCurrency, amount)))
        .andRespond(withBadRequest());

    currencyConverterService.convert(fromCurrency, targetCurrency, amount);
  }

  @Test(expected = BadRequestException.class)
  public void shouldNotConvertIfInvalidTargetCurrency() {
    double amount = 14.99;
    String fromCurrency = "USD";
    String targetCurrency = "GasfBP";
    server
        .expect(
            ExpectedCount.times(1),
            requestTo(
                MessageFormat.format(
                    currencyConvertEndpoint, fromCurrency, targetCurrency, amount)))
        .andRespond(withBadRequest());

    currencyConverterService.convert(fromCurrency, targetCurrency, amount);
  }

  @Test
  public void shouldReturnSupportedCurrencies() throws Exception {
    String symbolsJson =
        new String(
            Files.readAllBytes(
                Paths.get(getClass().getClassLoader().getResource("symbols.json").toURI())));
    server
        .expect(ExpectedCount.times(1), requestTo(currencySymbolsEndpoint))
        .andRespond(withSuccess(symbolsJson, MediaType.APPLICATION_JSON));

    List<String> supportedCurrencies = currencyConverterService.getSupportedCurrencies();
    assertThat(supportedCurrencies, equalTo(Arrays.asList("ALL", "XCD", "EUR", "BBD", "BTN", "BND", "XAF", "CUP", "USD")));
  }
}
