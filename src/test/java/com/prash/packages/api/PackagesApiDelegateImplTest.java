package com.prash.packages.api;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.prash.packages.api.repository.PackagesRepository;
import com.prash.packages.exception.BadRequestException;
import com.prash.packages.exception.NotFoundException;
import com.prash.packages.model.Currency;
import com.prash.packages.model.PackageModel;
import com.prash.packages.model.PackageRequest;
import com.prash.packages.model.Product;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.class)
public class PackagesApiDelegateImplTest {
  @InjectMocks private PackagesApiDelegateImpl packagesApiDelegate;
  @Mock private PackagesRepository packagesRepository;
  @Mock private ProductService productService;
  @Mock private CurrencyConverterService currencyConverterService;

  private Product product1 = new Product().id("VqKb4tyj9V6i").name("Shield").usdPrice(1149);
  private Product product2 = new Product().id("DXSQpv6XVeJm").name("Helmet").usdPrice(999);
  private PackageModel packageModel1 =
      new PackageModel()
          .name("Package A")
          .description("Package A descr")
          .addProductsItem(product1)
          .addProductsItem(product2);
  private PackageModel packageModel2 =
      new PackageModel().name("Package B").description("Package B descr").addProductsItem(product2);
  private PackageModel savedPackageModel =
      new PackageModel()
          .id(UUID.randomUUID().toString())
          .name("Package A")
          .description("Package A descr")
          .addProductsItem(product1)
          .addProductsItem(product2)
          .price(21.48);

  @Captor private ArgumentCaptor captor;

  @Test
  public void shouldCreateAPackage() {
    // given a package request
    PackageRequest request =
        new PackageRequest()
            .name(packageModel1.getName())
            .description(packageModel1.getDescription())
            .addProductIdsItem(product1.getId())
            .addProductIdsItem(product2.getId());
    List<@NotNull String> productIds = Arrays.asList(product1.getId(), product2.getId());
    Mockito.when(productService.getProducts(productIds))
        .thenReturn(Arrays.asList(product1, product2));

    Mockito.when(packagesRepository.insert(any(PackageModel.class))).thenReturn(savedPackageModel);

    // when package is created
    ResponseEntity<PackageModel> responseEntity = packagesApiDelegate.createPackage(request);

    // then package is created correctly
    assertThat(responseEntity.getBody().getName(), is(equalTo(request.getName())));
    assertThat(responseEntity.getBody().getDescription(), is(equalTo(request.getDescription())));
    assertThat(responseEntity.getBody().getPrice(), is(equalTo(21.48)));

    // and delegates are invoked correctly
    verify(productService).getProducts(Arrays.asList(product1.getId(), product2.getId()));
    verify(packagesRepository).insert(any(PackageModel.class));
  }

  @Test(expected = BadRequestException.class)
  public void shouldNotCreateAProductIfNoProducts() {
    // given a package request without products
    PackageRequest request =
        new PackageRequest()
            .name(packageModel1.getName())
            .description(packageModel1.getDescription());
    Mockito.when(productService.getProducts(any())).thenThrow(new BadRequestException(404, ""));

    // when package is created error should occur
    packagesApiDelegate.createPackage(request);

    fail("Expected error");
  }

  @Test
  public void shouldDeleteAPackage() {
    // given a package id
    List<@NotNull String> productIds = Arrays.asList(packageModel1.getId());

    // when package is created
    ResponseEntity<Void> responseEntity = packagesApiDelegate.deletePackage(product1.getId());

    // then delegates are invoked correctly
    verify(packagesRepository).deleteById(product1.getId());
  }

  @Test
  public void shouldListAllPackages() {
    // given two packages
    Mockito.when(packagesRepository.findAll())
        .thenReturn(Arrays.asList(packageModel1, packageModel2));

    // when package is listed
    ResponseEntity<List<PackageModel>> responseEntity = packagesApiDelegate.listPackages();

    // then delegates are invoked correctly
    verify(packagesRepository).findAll();
    assertThat(responseEntity.getBody().size(), is(equalTo(2)));
  }

  @Test(expected = NotFoundException.class)
  public void shouldNotUpdatePackageIfPackageNotFound() {
    PackageRequest request =
        new PackageRequest()
            .name(packageModel1.getName())
            .description(packageModel1.getDescription())
            .addProductIdsItem(product1.getId())
            .addProductIdsItem(product2.getId());

    // when package update is invoked with invalid package id error should be thrown
    packagesApiDelegate.updatePackage(request, "id");
    fail("Expected error");
  }

  @Test
  public void shouldUpdatePackage() {
    // given a saved package
    when(packagesRepository.findById(savedPackageModel.getId()))
        .thenReturn(Optional.of(savedPackageModel));

    PackageRequest request =
        new PackageRequest()
            .name("updated name")
            .description(packageModel1.getDescription())
            .addProductIdsItem(product2.getId());

    when(productService.getProducts(Arrays.asList(product2.getId())))
        .thenReturn(Arrays.asList(product2));

    // when package update is invoked
    ResponseEntity<PackageModel> responseEntity =
        packagesApiDelegate.updatePackage(request, savedPackageModel.getId());

    // then delegates are invoked correctly
    verify(packagesRepository).save(any(PackageModel.class));
  }

  @Test(expected = BadRequestException.class)
  public void shouldNotUpdatePackageIfProductNotFound() {
    when(packagesRepository.findById(savedPackageModel.getId()))
        .thenReturn(Optional.of(savedPackageModel));

    PackageRequest request =
        new PackageRequest()
            .name(packageModel1.getName())
            .description(packageModel1.getDescription())
            .addProductIdsItem(product2.getId());
    Mockito.when(productService.getProducts(Arrays.asList(product2.getId())))
        .thenThrow(new NotFoundException(404, ""));

    // when package update is invoked with invalid package id error should be thrown
    packagesApiDelegate.updatePackage(request, savedPackageModel.getId());
    fail("Expected error");
  }

  @Test
  public void shouldGetPackageInDefaultCurrencyPrice() {
    when(packagesRepository.findById(savedPackageModel.getId()))
        .thenReturn(Optional.of(savedPackageModel));
    packagesApiDelegate.setProductsApiBaseCurrency("USD");

    ResponseEntity<PackageModel> responseEntity =
        packagesApiDelegate.getPackage(savedPackageModel.getId(), "USD");

    verify(packagesRepository).findById(savedPackageModel.getId());
    verifyNoMoreInteractions(currencyConverterService);
  }

  @Test
  public void shouldGetPackageInSpecifiedCurrency() {
    when(packagesRepository.findById(savedPackageModel.getId()))
        .thenReturn(Optional.of(savedPackageModel));
    String baseCurrency = "USD";
    String targetCurrency = "GBP";
    double price = savedPackageModel.getPrice();

    when(currencyConverterService.convert(baseCurrency, targetCurrency, price)).thenReturn(100.0);
    packagesApiDelegate.setProductsApiBaseCurrency("USD");

    ResponseEntity<PackageModel> responseEntity =
        packagesApiDelegate.getPackage(savedPackageModel.getId(), "GBP");

    verify(packagesRepository).findById(savedPackageModel.getId());
    verify(currencyConverterService).convert(baseCurrency, targetCurrency, price);
    assertThat(responseEntity.getBody().getPrice(), is(equalTo(100.0)));
  }

  @Test(expected = NotFoundException.class)
  public void shouldNotGetPackageIfNotFound() {
    packagesApiDelegate.getPackage(savedPackageModel.getId(), "GBP");
    fail("expected error");
  }

  @Test(expected = BadRequestException.class)
  public void shouldNotGetPackageIfCurrencyIsNotValid() {
    when(packagesRepository.findById(savedPackageModel.getId()))
        .thenReturn(Optional.of(savedPackageModel));
    double price = savedPackageModel.getPrice();
    packagesApiDelegate.setProductsApiBaseCurrency("USD");

    when(currencyConverterService.convert("USD", "GBP", price))
        .thenThrow(new BadRequestException(400, ""));

    packagesApiDelegate.getPackage(savedPackageModel.getId(), "GBP");

    fail("expected error");
  }
}
