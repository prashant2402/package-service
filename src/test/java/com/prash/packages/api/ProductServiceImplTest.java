package com.prash.packages.api;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prash.packages.exception.NotFoundException;
import com.prash.packages.exception.RestTemplateResponseErrorHandler;
import com.prash.packages.invoker.PackageServiceApplication;
import com.prash.packages.model.Product;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PackageServiceApplication.class)
public class ProductServiceImplTest {

  private static final String PRODUCTS_URL =
      "https://product-service.herokuapp.com/api/v1/products";

  private MockRestServiceServer server;

  @Autowired
  private ProductServiceImpl productServiceImpl;

  @Autowired
  RestTemplateBuilder builder;

  private final String PRODUCT_1_ID = "VqKb4tyj9V6i";
  private Product product = new Product().id(PRODUCT_1_ID).name("Shield").usdPrice(1149);

  @Autowired private ObjectMapper objectMapper;

  @Before
  public void setUp() {
    server = MockRestServiceServer.createServer(productServiceImpl.getRestTemplate());
  }

  @Test
  public void shouldGetProductsFromApi() throws Exception {
    productServiceImpl.setProductsCache(new ConcurrentHashMap<>());

    server
        .expect(ExpectedCount.times(1), requestTo(PRODUCTS_URL + "/id/" + PRODUCT_1_ID))
        .andRespond(
            withSuccess(objectMapper.writeValueAsString(product), MediaType.APPLICATION_JSON));

    List<Product> products = productServiceImpl.getProducts(Arrays.asList(PRODUCT_1_ID));

    server.verify();

    assertThat(products.size(), is(equalTo(1)));
    assertThat(products.get(0).getId(), is(equalTo(PRODUCT_1_ID)));
  }

  @Test
  public void shouldListProductsFromApi() throws Exception {

    productServiceImpl.setProductsCache(new ConcurrentHashMap<>());

    String productsJson = new String(Files.readAllBytes(Paths.get(getClass().getClassLoader()
        .getResource("products.json").toURI())));

    server.expect(ExpectedCount.times(1), requestTo(PRODUCTS_URL))
        .andRespond(withSuccess(productsJson, MediaType.APPLICATION_JSON));

    List<Product> products = productServiceImpl.listProducts();

    server.verify();
    assertThat(products.isEmpty(), is(false));
    assertThat(products.size(), is(9));
  }

  @Test(expected = NotFoundException.class)
  public void shouldNotGetProductIfNotFound() throws Exception {
    server.expect(ExpectedCount.times(1), requestTo(PRODUCTS_URL + "/id/idontexist"))
        .andRespond(withStatus(HttpStatus.NOT_FOUND));

    productServiceImpl.getProducts(Arrays.asList("idontexist"));

    fail("error expected");
  }

  @Test
  public void shouldGetProductsFromCache() throws Exception {

    server
        .expect(ExpectedCount.times(1), requestTo(PRODUCTS_URL + "/id/" + PRODUCT_1_ID))
        .andRespond(
            withSuccess(objectMapper.writeValueAsString(product), MediaType.APPLICATION_JSON));

    productServiceImpl.getProducts(Arrays.asList(PRODUCT_1_ID));

    server.verify();

    MockRestServiceServer myserver = MockRestServiceServer.createServer(productServiceImpl.getRestTemplate());

    myserver
        .expect(ExpectedCount.never(), requestTo(PRODUCTS_URL + "/id/" + PRODUCT_1_ID))
        .andRespond(
            withSuccess(objectMapper.writeValueAsString(product), MediaType.APPLICATION_JSON));

    List<Product> products = productServiceImpl.getProducts(Arrays.asList(PRODUCT_1_ID));

    myserver.verify();

    assertThat(products.size(), is(equalTo(1)));
    assertThat(products.get(0).getId(), is(equalTo(PRODUCT_1_ID)));
  }
}
