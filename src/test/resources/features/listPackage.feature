Feature: 5.0 List packages
As a user of PackageService
I want to list packages.

  Scenario: 5.1 List packages is empty
    Given there are no packages
     When list packages is invoked
     Then no packages are returned
      And response status is 200

  Scenario: 5.2 List packages returns packages correctly
    Given there are packages `package A, package B`
     When list packages is invoked
     Then packages `package A, package B` are returned
      And response status is 200

