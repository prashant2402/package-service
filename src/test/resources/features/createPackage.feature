Feature: 1.0 Create a package
As a user of PackageService
I want to create a package containing one or more valid products

  Scenario Outline: 1.1 Create a package for a request containing valid products
    Given a package with name `<name>`, description `<description>` and productIds `<product id>`
     When create package is invoked
     Then the package `<name>` is created with products `<products>` and price `<price>`
    Examples:
      | name     | description    | product id                                             | products                                                                                                                                                                                                                    | price |
      | packageA | my description | VqKb4tyj9V6i                                           | [{"id":"VqKb4tyj9V6i","name":"Shield","usdPrice":1149}]                                                                                                                                                                     | 11.49  |
      | packageB | my description | PKM5pGAh9yGm, 7Hv0hA2nmci7, 500R5EHvNlNB, IP3cv7TcZhQn | [{"id":"PKM5pGAh9yGm","name":"Axe","usdPrice":799},{"id":"7Hv0hA2nmci7","name":"Knife","usdPrice":349},{"id":"500R5EHvNlNB","name":"Gold Coin","usdPrice":249},{"id":"IP3cv7TcZhQn","name":"Platinum Coin","usdPrice":399}] | 17.96  |

  Scenario Outline: 1.2 Create package request fails for an invalid request
    Given a package with name `<name>`, description `<description>` and productIds `<product id>`
     When create package is invoked
     Then request fails with status code 400
    Examples:
      | comments                               | name     | description    | product id                                                                                                         |
      | package doesn't contain any product Id | packageA | my description |                                                                                                                    |
      | package contains an invalid product Id | packageB | my description | [{"id":"i dont exist","name":"invalid product","usdPrice":0},{"id":"DXSQpv6XVeJm","name":"Helmet","usdPrice":999}] |


