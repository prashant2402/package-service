Feature: 2.0 Retrieve a package
As a user of PackageService
I want to retrieve a package in a specified currency using the latest foreign exchange rate. The API should
default to USD if no currency is provided.

  Scenario Outline: 2.1 Retrieve a package using default currency USD
    Given there is a package with name `<name>`, description `<description>` and products `<products>`
     When retrieve package is invoked
     Then the package is retrieved with name `<name>`, description `<description>`, products `<product id>` and price `<price>`
      And response status is 200
    Examples:
      | name           | description         | products                                                                                                                                                                                                                    | price |
      | Shield Package | Package for shields | [{"id":"VqKb4tyj9V6i","name":"Shield","usdPrice":1149}]                                                                                                                                                                     | 11.49  |
      | Tools Package  | Package for tools   | [{"id":"PKM5pGAh9yGm","name":"Axe","usdPrice":799},{"id":"7Hv0hA2nmci7","name":"Knife","usdPrice":349},{"id":"500R5EHvNlNB","name":"Gold Coin","usdPrice":249},{"id":"IP3cv7TcZhQn","name":"Platinum Coin","usdPrice":399}] | 17.96  |

  Scenario Outline: 2.2 Retrieve a package in a specified currency using the latest foreign exchange rate
    Given there is a package with name `<name>`, description `<description>` and products `<products>`
     When retrieve package is invoked in currency `GBP`
     Then the package is retrieved with name `<name>`, description `<description>`, products `<products>`
      And price is in `GBP` calculated using latest forex rate
    Examples:
      | name           | description         | products                                                                                                                                                                                                                    |
      | Shield Package | Package for shields | [{"id":"VqKb4tyj9V6i","name":"Shield","usdPrice":1149}]                                                                                                                                                                     |
      | Tools Package  | Package for tools   | [{"id":"PKM5pGAh9yGm","name":"Axe","usdPrice":799},{"id":"7Hv0hA2nmci7","name":"Knife","usdPrice":349},{"id":"500R5EHvNlNB","name":"Gold Coin","usdPrice":249},{"id":"IP3cv7TcZhQn","name":"Platinum Coin","usdPrice":399}] |

  Scenario: 2.3 Package cannot be retrieved if the requested package doesn't exist
    Given there is no package with package id `nonexistingpackage`
     When retrieve package is invoked with id `nonexistingpackage`
     Then request fails with status code 404

  Scenario: 2.4 Package cannot be retrieved if the requested currency doesn't exist
    Given there is a package with name `Shield Package`, description `Package for shields` and products `[{"id":"VqKb4tyj9V6i","name":"Shield","usdPrice":1149}]`
     When retrieve package is invoked in currency `XXX`
     Then request fails with status code 400

