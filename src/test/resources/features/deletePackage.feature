Feature: 4.0 Delete a package
As a user of PackageService
I want to delete a package.

  Scenario: 4.1 Deletes a package
    Given there is a package with name `mypackage` and product id `VqKb4tyj9V6i`
     When delete package is invoked
     Then the package `mypackage` is deleted

  Scenario: 4.2 Delete package if package is not found
    Given there is no package with package id `nonexistingpackage`
     When delete package is invoked with id `nonexistingpackage`
     Then response status is 204


