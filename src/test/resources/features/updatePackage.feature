Feature: 3.0 Update a package
As a user of PackageService
I want to update a package.

  Scenario Outline: 3.1 Update a package for a single attribute
    Given there is a package with name `<name>`, description `<description>` and products `<products>`
     When update package is invoked with name `<updated name>`
     Then the package is updated with name `<updated name>`, description `<description>`, products `<product id>` and price `<price>`
      And response status is 200
    Examples:
      | updated name           | name           | description         | products                                                | price |
      | Shield Package updated | Shield Package | Package for shields | [{"id":"VqKb4tyj9V6i","name":"Shield","usdPrice":1149}] | 1149  |

  Scenario Outline: 3.2 Update a package with new products
    Given there is a package with name `<name>`, description `<description>` and products `<products>`
     When update package is invoked with products `<updated products>`
     Then the package is updated with products `<updated products>` and price `<price>`
      And response status is 200
    Examples:
      | name           | description         | products                                                | updated products                                                                                           | price |
      | Shield Package | Package for shields | [{"id":"VqKb4tyj9V6i","name":"Shield","usdPrice":1149}] | [{"id":"VqKb4tyj9V6i","name":"Shield","usdPrice":1149}, {"id":"PKM5pGAh9yGm","name":"Axe","usdPrice":799}] | 1948  |

  Scenario: 3.3 Package cannot be updated if the requested package doesn't exist
    Given there is no package with package id `nonexistingpackage`
     When update package is invoked with name `updated name`
     Then request fails with status code 404

  Scenario: 3.4 Package cannot be updated if the updated product is not valid
    Given there is a valid package and an invalid product `idontexist`
     When update package is invoked with name `idontexist`
     Then request fails with status code 400

  Scenario Outline: 3.5 Update a package with same attributes doesn't fail (for idempotent)
    Given there is a package with name `<name>`, description `<description>` and products `<products>`
     When update package is invoked with name `<name>`, description `<description>` and products `<products>`
     Then the package is updated with name `<updated name>`, description `<description>`, products `<product id>` and price `<price>`
      And response status is 200
    Examples:
      | name           | description         | products                                                | price |
      | Shield Package | Package for shields | [{"id":"VqKb4tyj9V6i","name":"Shield","usdPrice":1149}] | 1149  |

