(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _packages_packages_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./packages/packages.component */ "./src/app/packages/packages.component.ts");
/* harmony import */ var _package_create_package_create_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./package-create/package-create.component */ "./src/app/package-create/package-create.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    { path: 'packages', component: _packages_packages_component__WEBPACK_IMPORTED_MODULE_2__["PackagesComponent"] },
    {
        path: 'package-create',
        component: _package_create_package_create_component__WEBPACK_IMPORTED_MODULE_3__["PackageCreateComponent"]
    },
    { path: '', redirectTo: 'packages', pathMatch: 'full' }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes, { useHash: true })],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<img id=\"logo\" src=\"assets/logo.png\" border=\"0\" width=\"364\" height=\"126\"/>\n\n<!-- navs -->\n<ul class=\"nav\">\n  <li class=\"nav-item\">\n    <a class=\"nav-link\" routerLink=\"packages\">Packages</a>\n  </li>\n  <li class=\"nav-item\">\n    <a class=\"nav-link\" routerLink=\"package-create\">Create Package</a>\n  </li>\n</ul>\n\n<!-- breadcrumb -->\n<div class=\"col\">\n  <ol class=\"breadcrumb\">\n    <li class=\"breadcrumb-item\"\n        *ngFor=\"let item of breadcrumbList; let i = index\"\n        [class.active]=\"i===breadcrumbList.length-1\">\n      <a [routerLink]=\"item.path\" *ngIf=\"i!==breadcrumbList.length-1\">\n        {{ item.name }}\n      </a>\n      <span *ngIf=\"i===breadcrumbList.length-1\">{{ item.name }}</span>\n    </li>\n  </ol>\n</div>\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _menu_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./menu.service */ "./src/app/menu.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = /** @class */ (function () {
    function AppComponent(_router, menuService) {
        this._router = _router;
        this.menuService = menuService;
        this.title = 'frontend';
        this.menu = [];
        this.breadcrumbList = [];
    }
    AppComponent.prototype.ngOnInit = function () {
        this.menu = this.menuService.getMenu();
        this.listenRouting();
    };
    AppComponent.prototype.listenRouting = function () {
        var _this = this;
        var routerUrl, routerList, target;
        this._router.events.subscribe(function (router) {
            routerUrl = router.urlAfterRedirects;
            if (routerUrl && typeof routerUrl === 'string') {
                target = _this.menu;
                _this.breadcrumbList.length = 0;
                routerList = routerUrl.slice(1).split('/');
                routerList.forEach(function (router, index) {
                    target = target.find(function (page) { return page.path.slice(2) === router; });
                    _this.breadcrumbList.push({
                        name: target.name,
                        path: (index === 0) ? target.path : _this.breadcrumbList[index - 1].path + "/" + target.path.slice(2)
                    });
                    if (index + 1 !== routerList.length) {
                        target = target.children;
                    }
                });
                console.log(_this.breadcrumbList);
            }
        });
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _menu_service__WEBPACK_IMPORTED_MODULE_2__["MenuService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _packages_packages_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./packages/packages.component */ "./src/app/packages/packages.component.ts");
/* harmony import */ var _package_detail_package_detail_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./package-detail/package-detail.component */ "./src/app/package-detail/package-detail.component.ts");
/* harmony import */ var _package_create_package_create_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./package-create/package-create.component */ "./src/app/package-create/package-create.component.ts");
/* harmony import */ var _package_update_package_update_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./package-update/package-update.component */ "./src/app/package-update/package-update.component.ts");
/* harmony import */ var _menu_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./menu.service */ "./src/app/menu.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var appRoutes = [
    {
        path: 'packages',
        component: _packages_packages_component__WEBPACK_IMPORTED_MODULE_7__["PackagesComponent"],
        data: { title: 'Packages' }
    },
    {
        path: 'package-detail/:id',
        component: _package_detail_package_detail_component__WEBPACK_IMPORTED_MODULE_8__["PackageDetailComponent"],
        data: { title: 'Package Details' }
    },
    {
        path: 'package-create',
        component: _package_create_package_create_component__WEBPACK_IMPORTED_MODULE_9__["PackageCreateComponent"],
        data: { title: 'Create Package' }
    },
    {
        path: 'package-update/:id',
        component: _package_update_package_update_component__WEBPACK_IMPORTED_MODULE_10__["PackageUpdateComponent"],
        data: { title: 'Update Package' }
    },
    { path: '',
        redirectTo: '/packages',
        pathMatch: 'full'
    }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
                _packages_packages_component__WEBPACK_IMPORTED_MODULE_7__["PackagesComponent"],
                _package_detail_package_detail_component__WEBPACK_IMPORTED_MODULE_8__["PackageDetailComponent"],
                _package_create_package_create_component__WEBPACK_IMPORTED_MODULE_9__["PackageCreateComponent"],
                _package_update_package_update_component__WEBPACK_IMPORTED_MODULE_10__["PackageUpdateComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forRoot(appRoutes, { enableTracing: true } // <-- debugging purposes only
                )
            ],
            providers: [_menu_service__WEBPACK_IMPORTED_MODULE_11__["MenuService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/menu.service.ts":
/*!*********************************!*\
  !*** ./src/app/menu.service.ts ***!
  \*********************************/
/*! exports provided: MenuService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuService", function() { return MenuService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MenuService = /** @class */ (function () {
    function MenuService() {
    }
    MenuService.prototype.getMenu = function () {
        var menu = [
            { name: 'packages', path: './packages', children: [] },
            { name: 'package-create', path: './package-create', children: [] },
        ];
        return menu;
    };
    MenuService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], MenuService);
    return MenuService;
}());



/***/ }),

/***/ "./src/app/package-create/package-create.component.css":
/*!*************************************************************!*\
  !*** ./src/app/package-create/package-create.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/package-create/package-create.component.html":
/*!**************************************************************!*\
  !*** ./src/app/package-create/package-create.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <h1>Add New Package</h1>\n  <div class=\"row\">\n    <div class=\"col-md-6\">\n      <form (ngSubmit)=\"savePackage()\" #packageForm=\"ngForm\">\n        <div class=\"form-group\">\n          <label for=\"name\">Name</label>\n          <input type=\"text\" class=\"form-control\" [(ngModel)]=\"productpackage.name\" name=\"name\"\n                 required>\n        </div>\n        <div class=\"form-group\">\n          <label for=\"name\">Description</label>\n          <input type=\"text\" class=\"form-control\" [(ngModel)]=\"productpackage.description\"\n                 name=\"description\" required>\n        </div>\n\n        <div class=\"form-group\" *ngFor=\"let product of productpackage.products; let i = index\">\n          <label for=\"name\">Product Id</label>\n          <input type=\"text\" placeholder=\"Enter a valid Product Id\" name=\"product-{{i}}\"\n                 class=\"form-control\" [(ngModel)]=\"productpackage.products[i].value\" required>\n          <button type=\"button\" (click)=\"removeProduct(product)\" aria-label=\"Remove\">X</button>\n        </div>\n        <button class=\"btn btn-danger\" type=\"button\" (click)=\"addProduct()\">Add new product</button>\n\n        <div class=\"form-group\">\n          <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!packageForm.form.valid\">Save\n          </button>\n        </div>\n        <div *ngIf=\"errors\" class=\"alert alert-danger\">\n          {{ errors }}\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/package-create/package-create.component.ts":
/*!************************************************************!*\
  !*** ./src/app/package-create/package-create.component.ts ***!
  \************************************************************/
/*! exports provided: PackageCreateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PackageCreateComponent", function() { return PackageCreateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PackageCreateComponent = /** @class */ (function () {
    function PackageCreateComponent(http, router) {
        this.http = http;
        this.router = router;
        this.apiUrl = 'http://localhost:8080/api/v1/packages';
        this.products = [];
        this.productpackage = { products: this.products, name: this.name, description: this.description };
    }
    PackageCreateComponent.prototype.ngOnInit = function () {
    };
    PackageCreateComponent.prototype.savePackage = function () {
        var _this = this;
        var productIds = this.productpackage.products.map(function (a) { return a.value; });
        this.productpackage['productIds'] = productIds;
        this.http.post(this.apiUrl, this.productpackage)
            .subscribe(function (res) {
            _this.router.navigate(['/packages', res]);
        }, function (err) {
            _this.errors = err.error.status + ':' + err.error.message;
        });
    };
    PackageCreateComponent.prototype.addProduct = function () {
        this.productpackage.products.push({ value: 'Enter a valid Product Id' });
    };
    ;
    PackageCreateComponent.prototype.removeProduct = function (productToRemove) {
        var index = this.productpackage.products.indexOf(productToRemove);
        this.productpackage.products.splice(index, 1);
    };
    ;
    PackageCreateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-package-create',
            template: __webpack_require__(/*! ./package-create.component.html */ "./src/app/package-create/package-create.component.html"),
            styles: [__webpack_require__(/*! ./package-create.component.css */ "./src/app/package-create/package-create.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], PackageCreateComponent);
    return PackageCreateComponent;
}());



/***/ }),

/***/ "./src/app/package-detail/package-detail.component.css":
/*!*************************************************************!*\
  !*** ./src/app/package-detail/package-detail.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/package-detail/package-detail.component.html":
/*!**************************************************************!*\
  !*** ./src/app/package-detail/package-detail.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <h1>{{ productpackage.name }}</h1>\n  <dl class=\"list\">\n    <dt>Description</dt>\n    <dd>{{ productpackage.description }}</dd>\n    <dt>Products</dt>\n    <dd>\n      <div class=\"row\" *ngFor=\"let product of productpackage.products; let i = index\">\n        <div class=\"col-md-7\">\n          {{ product.name }}\n        </div>\n      </div>\n    </dd>\n    <dt>Price</dt>\n    <dd>{{ priceString }}</dd>\n  </dl>\n\n  <label>Currency: </label>\n  <select (change)=\"getPackageDetailInCurrency(productpackage.id, $event.target.value)\">\n    <option *ngFor=\"let currency of currencies\" value={{currency}}>\n      {{currency}}\n    </option>\n  </select>\n\n  <div class=\"row\">\n    <div class=\"col-md-12\">\n      <a [routerLink]=\"['/package-update', productpackage.id]\" class=\"btn btn-success\">UPDATE</a>\n      &nbsp;\n       <button class=\"btn btn-danger\" type=\"button\" (click)=\"deletePackage(productpackage.id)\">DELETE</button>\n    </div>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/package-detail/package-detail.component.ts":
/*!************************************************************!*\
  !*** ./src/app/package-detail/package-detail.component.ts ***!
  \************************************************************/
/*! exports provided: PackageDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PackageDetailComponent", function() { return PackageDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PackageDetailComponent = /** @class */ (function () {
    function PackageDetailComponent(router, route, http, location) {
        this.router = router;
        this.route = route;
        this.http = http;
        this.location = location;
        this.apiUrl = 'http://localhost:8080/api/v1/packages/';
        this.currencyUrl = 'http://localhost:8080/api/v1/currency';
        this.currency = 'USD';
        this.priceString = '';
    }
    PackageDetailComponent.prototype.ngOnInit = function () {
        this.getPackageDetail(this.route.snapshot.params['id']);
        this.getCurrencies();
    };
    PackageDetailComponent.prototype.getPackageDetail = function (id) {
        var _this = this;
        this.http.get(this.apiUrl + id).subscribe(function (data) {
            _this.productpackage = data;
            _this.priceString = _this.productpackage.price + ' ' + _this.currency;
        });
    };
    PackageDetailComponent.prototype.getPackageDetailInCurrency = function (id, currency) {
        var _this = this;
        this.currency = currency;
        this.http.get(this.apiUrl + id + '?currency=' + currency).subscribe(function (data) {
            _this.productpackage = data;
            _this.priceString = _this.productpackage.price + ' ' + _this.currency;
        });
    };
    PackageDetailComponent.prototype.getCurrencies = function () {
        var _this = this;
        this.http.get(this.currencyUrl).subscribe(function (data) {
            _this.currencies = data;
        });
    };
    PackageDetailComponent.prototype.deletePackage = function (id) {
        var _this = this;
        this.http.delete(this.apiUrl + id)
            .subscribe(function (res) {
            _this.router.navigate(['/packages']);
        }, function (err) {
            _this.errors = err.error.status + ':' + err.error.message;
        });
    };
    PackageDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-package-detail',
            template: __webpack_require__(/*! ./package-detail.component.html */ "./src/app/package-detail/package-detail.component.html"),
            styles: [__webpack_require__(/*! ./package-detail.component.css */ "./src/app/package-detail/package-detail.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"]])
    ], PackageDetailComponent);
    return PackageDetailComponent;
}());



/***/ }),

/***/ "./src/app/package-update/package-update.component.css":
/*!*************************************************************!*\
  !*** ./src/app/package-update/package-update.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/package-update/package-update.component.html":
/*!**************************************************************!*\
  !*** ./src/app/package-update/package-update.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <h1>Update Package</h1>\n  <div class=\"row\">\n    <div class=\"col-md-6\">\n      <form (ngSubmit)=\"updatePackage(productpackage.id)\" #packageForm=\"ngForm\">\n        <div class=\"form-group\">\n          <label for=\"name\">Name</label>\n          <input type=\"text\" class=\"form-control\" [(ngModel)]=\"productpackage.name\" name=\"name\" required>\n        </div>\n        <div class=\"form-group\">\n          <label for=\"name\">Description</label>\n          <input type=\"text\" class=\"form-control\" [(ngModel)]=\"productpackage.description\" name=\"description\" required>\n        </div>\n        <div class=\"form-group\" *ngFor=\"let product of productpackage.products; let i = index\">\n          <label for=\"name\">Product Id</label>\n          <input type=\"text\" placeholder=\"Enter a valid Product Id\" name=\"product-{{i}}\"\n                 class=\"form-control\" [(ngModel)]=\"product.id\">\n          <button type=\"button\" (click)=\"removeProduct(product)\" aria-label=\"Remove\">X</button>\n        </div>\n        <button class=\"btn btn-danger\" type=\"button\" (click)=\"addProduct()\">Add new product</button>\n        <div class=\"form-group\">\n          <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!packageForm.form.valid\">Update</button>\n        </div>\n        <div *ngIf=\"errors\" class=\"alert alert-danger\">\n          {{ errors }}\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/package-update/package-update.component.ts":
/*!************************************************************!*\
  !*** ./src/app/package-update/package-update.component.ts ***!
  \************************************************************/
/*! exports provided: PackageUpdateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PackageUpdateComponent", function() { return PackageUpdateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PackageUpdateComponent = /** @class */ (function () {
    function PackageUpdateComponent(http, router, route) {
        this.http = http;
        this.router = router;
        this.route = route;
        this.apiUrl = 'http://localhost:8080/api/v1/packages/';
        this.productsIds = [];
    }
    PackageUpdateComponent.prototype.ngOnInit = function () {
        this.getPackage(this.route.snapshot.params['id']);
    };
    PackageUpdateComponent.prototype.getPackage = function (id) {
        var _this = this;
        this.http.get(this.apiUrl + id).subscribe(function (data) {
            _this.productpackage = data;
        });
    };
    PackageUpdateComponent.prototype.updatePackage = function (id) {
        var _this = this;
        var productIds = this.productpackage.products.map(function (a) { return a.id; });
        this.productpackage['productIds'] = productIds;
        this.http.put(this.apiUrl + id, this.productpackage)
            .subscribe(function (res) {
            var id = res['id'];
            _this.router.navigate(['/package-detail', id], { skipLocationChange: true });
        }, function (err) {
            _this.errors = err.error.status + ':' + err.error.message;
        });
    };
    PackageUpdateComponent.prototype.addProduct = function () {
        this.productpackage.products.push({ value: 'Enter a valid Product Id' });
    };
    ;
    PackageUpdateComponent.prototype.removeProduct = function (productToRemove) {
        var index = this.productpackage.products.indexOf(productToRemove);
        this.productpackage.products.splice(index, 1);
    };
    ;
    PackageUpdateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-package-update',
            template: __webpack_require__(/*! ./package-update.component.html */ "./src/app/package-update/package-update.component.html"),
            styles: [__webpack_require__(/*! ./package-update.component.css */ "./src/app/package-update/package-update.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], PackageUpdateComponent);
    return PackageUpdateComponent;
}());



/***/ }),

/***/ "./src/app/packages/packages.component.css":
/*!*************************************************!*\
  !*** ./src/app/packages/packages.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/packages/packages.component.html":
/*!**************************************************!*\
  !*** ./src/app/packages/packages.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <h1>Packages</h1>\n  <table class=\"table\">\n    <thead>\n    <tr>\n      <th>Name</th>\n      <th>Description</th>\n      <th>Products</th>\n      <th>Price</th>\n    </tr>\n    </thead>\n    <tbody>\n    <tr *ngFor=\"let package of productpackages\">\n      <td>{{ package.name }}</td>\n      <td>{{ package.description }}</td>\n      <td>\n        <div class=\"row\" *ngFor=\"let product of package.products; let i = index\">\n          <div class=\"col-md-7\">\n            {{ product.name }}\n          </div>\n        </div>\n      </td>\n      <td>{{ package.price }}</td>\n      <td><a [routerLink]=\"['/package-detail', package.id]\">Show Detail</a></td>\n    </tr>\n    </tbody>\n  </table>\n</div>\n"

/***/ }),

/***/ "./src/app/packages/packages.component.ts":
/*!************************************************!*\
  !*** ./src/app/packages/packages.component.ts ***!
  \************************************************/
/*! exports provided: PackagesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PackagesComponent", function() { return PackagesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PackagesComponent = /** @class */ (function () {
    function PackagesComponent(http) {
        this.http = http;
        this.apiUrl = 'http://localhost:8080/api/v1/packages';
    }
    PackagesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.http.get(this.apiUrl).subscribe(function (data) {
            _this.productpackages = data;
        });
    };
    PackagesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-packages',
            template: __webpack_require__(/*! ./packages.component.html */ "./src/app/packages/packages.component.html"),
            styles: [__webpack_require__(/*! ./packages.component.css */ "./src/app/packages/packages.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], PackagesComponent);
    return PackagesComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/prash/workspace/package-service/frontend/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map