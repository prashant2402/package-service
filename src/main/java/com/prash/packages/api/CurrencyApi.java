/**
 * NOTE: This class is auto generated by the swagger code generator program (3.0.0-SNAPSHOT).
 * https://github.com/swagger-api/swagger-codegen Do not edit the class manually.
 */
package com.prash.packages.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.java.SpringCodegen",
    date = "2018-10-01T16:51:06.712+01:00[Europe/London]")
@Api(value = "currency", description = "the currency API")
public interface CurrencyApi {

  CurrencyApiDelegate getDelegate();

  @ApiOperation(
      value = "List all supported currencies",
      nickname = "getCurrencies",
      notes = "List all supported currencies",
      response = String.class,
      responseContainer = "List",
      tags = {
        "currency",
      })
  @ApiResponses(
      value = {
        @ApiResponse(
            code = 200,
            message = "A list of supported currencies",
            response = String.class,
            responseContainer = "List")
      })
  @RequestMapping(
      value = "/currency",
      produces = {"application/json"},
      method = RequestMethod.GET)
  default ResponseEntity<List<String>> getCurrencies() {

    return getDelegate().getCurrencies();
  }
}
