package com.prash.packages.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.java.SpringCodegen",
    date = "2018-10-01T16:51:06.712+01:00[Europe/London]")
@Controller
@CrossOrigin("http://localhost:4200")
public class CurrencyApiController implements CurrencyApi {

  private final CurrencyApiDelegate delegate;

  @org.springframework.beans.factory.annotation.Autowired
  public CurrencyApiController(CurrencyApiDelegate delegate) {
    this.delegate = delegate;
  }

  @Override
  public CurrencyApiDelegate getDelegate() {
    return delegate;
  }
}
