package com.prash.packages.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CurrencyApiDelegateImpl implements CurrencyApiDelegate {

  @Autowired CurrencyConverterService currencyConverterService;

  @Override
  public ResponseEntity<List<String>> getCurrencies() {
    List<String> currencies = currencyConverterService.getSupportedCurrencies();
    return new ResponseEntity<>(currencies, HttpStatus.OK);
  }
}
