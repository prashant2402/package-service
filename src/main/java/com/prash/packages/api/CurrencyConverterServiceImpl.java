package com.prash.packages.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.annotations.VisibleForTesting;
import com.prash.packages.exception.BadRequestException;
import com.prash.packages.exception.RestTemplateResponseErrorHandler;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CurrencyConverterServiceImpl implements CurrencyConverterService {

  private static final ObjectMapper MAPPER = new ObjectMapper();
  Logger logger = LoggerFactory.getLogger(CurrencyConverterService.class);

  @Value("${currencyconverterapi.convert}")
  private String currencyConvertEndpoint;

  @Value("${currencyconverterapi.symbols}")
  private String currencySymbolsEndpoint;

  private RestTemplate restTemplate;

  public CurrencyConverterServiceImpl(RestTemplateBuilder builder) {
    this.restTemplate = builder.errorHandler(new RestTemplateResponseErrorHandler()).build();
  }

  public double convert(final String baseCurrency, final String targetCurrency, double amount) {
    ResponseEntity<String> response =
        restTemplate.getForEntity(
            MessageFormat.format(currencyConvertEndpoint, baseCurrency, targetCurrency),
            String.class);
    try {
      JsonNode root = MAPPER.readTree(response.getBody());
      JsonNode name = root.path(baseCurrency + "_" + targetCurrency);
      if (name != null && StringUtils.isNotEmpty(name.asText())) {
        return Double.valueOf(name.asText()) * amount;
      }
    } catch (IOException e) {
      logger.error("Error while converting currency: " + e.getMessage());
    }
    throw new BadRequestException(
        400, "Cannot convert " + amount + "from " + baseCurrency + " to " + targetCurrency);
  }

  @Override
  public List<String> getSupportedCurrencies() {
    ResponseEntity<String> response =
        restTemplate.getForEntity(currencySymbolsEndpoint, String.class);
    if (response.getStatusCodeValue() == 200) {
      try {
        List<String> currencies = new ArrayList<>();
        JsonNode root = MAPPER.readTree(response.getBody());
        JsonNode resultsNode = root.path("results");
        Consumer<JsonNode> data =
            (JsonNode node) -> {
              JsonNode currencySymbol = node.findValue("id");
              if (currencySymbol != null) {
                currencies.add(currencySymbol.asText());
              }
            };
        resultsNode.forEach(data);

        return currencies;
      } catch (IOException e) {
        logger.error("Error while fetching currency symbols: " + e.getMessage());
      }
    }
    throw new BadRequestException(
        400, response.getStatusCodeValue() + " http error while fetching symbols");
  }

  @VisibleForTesting
  public RestTemplate getRestTemplate() {
    return restTemplate;
  }
}
