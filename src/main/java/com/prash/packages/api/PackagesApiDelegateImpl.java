package com.prash.packages.api;

import com.google.common.annotations.VisibleForTesting;
import com.prash.packages.api.repository.PackagesRepository;
import com.prash.packages.exception.BadRequestException;
import com.prash.packages.exception.NotFoundException;
import com.prash.packages.model.PackageModel;
import com.prash.packages.model.PackageRequest;
import com.prash.packages.model.Product;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PackagesApiDelegateImpl implements PackagesApiDelegate {

  @Autowired PackagesRepository packagesRepository;

  @Autowired ProductService productService;

  @Autowired CurrencyConverterService currencyConverterService;

  @Value("${products.api.base.currency}")
  private String productsApiBaseCurrency;

  @Override
  public ResponseEntity<PackageModel> createPackage(PackageRequest body) {
    List<Product> products;
    try {
      products = productService.getProducts(body.getProductIds());
    } catch (NotFoundException e) {
      throw new BadRequestException(
          400, "One or more invalid products given: " + body.getProductIds());
    }
    PackageModel packageModel =
        new PackageModel()
            .name(body.getName())
            .description(body.getDescription())
            .products(products);
    packageModel.price(packageModel.calculateTotalPrice());
    PackageModel savedPackageModel = packagesRepository.insert(packageModel);
    return new ResponseEntity<>(savedPackageModel, HttpStatus.CREATED);
  }

  @Override
  public ResponseEntity<Void> deletePackage(String packageId) {
    packagesRepository.deleteById(packageId);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  @Override
  public ResponseEntity<List<PackageModel>> listPackages() {
    return new ResponseEntity<>(packagesRepository.findAll(), HttpStatus.OK);
  }

  @Override
  public ResponseEntity<PackageModel> updatePackage(PackageRequest body, String packageId) {
    Optional<PackageModel> packageModel = packagesRepository.findById(packageId);
    if (!packageModel.isPresent()) {
      throw new NotFoundException(404, "package id" + packageId + " cannot be found");
    }
    boolean shouldUpdate = false;
    if (!body.getName().equals(packageModel.get().getName())) {
      packageModel.get().name(body.getName());
      shouldUpdate = true;
    }
    if (!body.getDescription().equals(packageModel.get().getDescription())) {
      packageModel.get().description(body.getDescription());
      shouldUpdate = true;
    }
    List<String> currentProductIds =
        packageModel.get().getProducts().stream().map(Product::getId).collect(Collectors.toList());
    List<String> newProductIds = body.getProductIds();
    if (!currentProductIds.equals(newProductIds)) {
      List<Product> products;
      try {
        products = productService.getProducts(body.getProductIds());
      } catch (NotFoundException e) {
        throw new BadRequestException(
            400, "One or more invalid products given: " + body.getProductIds());
      }
      packageModel.get().products(products);
      packageModel.get().price(packageModel.get().calculateTotalPrice());
      shouldUpdate = true;
    }
    if (shouldUpdate) {
      return new ResponseEntity<>(packagesRepository.save(packageModel.get()), HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @Override
  public ResponseEntity<PackageModel> getPackage(final String packageId, final String currency) {
    Optional<PackageModel> packageModel = packagesRepository.findById(packageId);
    if (!packageModel.isPresent()) {
      throw new NotFoundException(404, "package id" + packageId + " cannot be found");
    }

    if (currency != null && !currency.equalsIgnoreCase(productsApiBaseCurrency)) {
      double currencyResponse =
          currencyConverterService.convert(
              productsApiBaseCurrency, currency, packageModel.get().getPrice());
      double roundedPrice =
          new BigDecimal(currencyResponse).setScale(2, RoundingMode.HALF_UP).doubleValue();
      packageModel.get().price(roundedPrice);
    }
    return new ResponseEntity<>(packageModel.get(), HttpStatus.OK);
  }

  @VisibleForTesting
  public void setProductsApiBaseCurrency(String productsApiBaseCurrency) {
    this.productsApiBaseCurrency = productsApiBaseCurrency;
  }
}
