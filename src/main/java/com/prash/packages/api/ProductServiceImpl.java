package com.prash.packages.api;

import com.google.common.annotations.VisibleForTesting;
import com.prash.packages.exception.RestTemplateResponseErrorHandler;
import com.prash.packages.model.Product;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ProductServiceImpl implements ProductService {

  protected static final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

  private ConcurrentHashMap<String, Product> productsCache = new ConcurrentHashMap<>();

  @Autowired private CurrencyConverterService currencyConverterService;

  @Value("${products.api.baseurl}")
  private String productsApiBaseUrl;

  @Value("${products.api.endpoint.products}")
  private String productsApiEndpointProducts;

  @Value("${products.api.user}")
  private String productsApiUser;

  @Value("${products.api.password}")
  private String productsApiPassword;

  private RestTemplate restTemplate;

  public ProductServiceImpl(RestTemplateBuilder builder) {
    this.restTemplate = builder.errorHandler(new RestTemplateResponseErrorHandler()).build();
  }

  @PostConstruct
  public void init() {
    List<Product> products = listProducts();
    updateProductsCache(products);
  }

  private void updateProductsCache(List<Product> productDTOS) {
    productDTOS.forEach(entity -> productsCache.put(entity.getId(), entity));
  }

  @Override
  public List<Product> listProducts() {
    restTemplate
        .getInterceptors()
        .add(new BasicAuthorizationInterceptor(productsApiUser, productsApiPassword));
    ResponseEntity<List<Product>> responseEntity =
        restTemplate.exchange(
            productsApiBaseUrl + productsApiEndpointProducts,
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<List<Product>>() {});
    return responseEntity.getBody();
  }

  public List<Product> getProducts(List<String> productIds) {
    return productIds
        .stream()
        .map(
            productId -> {
              if (productsCache.containsKey(productId)) {
                return productsCache.get(productId);
              }
              return getProductFromApi(productId);
            })
        .collect(Collectors.toList());
  }

  private Product getProductFromApi(String id) {
    restTemplate
        .getInterceptors()
        .add(new BasicAuthorizationInterceptor(productsApiUser, productsApiPassword));
    ResponseEntity<Product> responseEntity =
        restTemplate.exchange(
            productsApiBaseUrl + productsApiEndpointProducts + "/id/" + id,
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<Product>() {});
    Product product = responseEntity.getBody();
    productsCache.put(id, product);
    return product;
  }

  @VisibleForTesting
  public void setProductsCache(ConcurrentHashMap<String, Product> productsCache) {
    this.productsCache = productsCache;
  }

  @VisibleForTesting
  public RestTemplate getRestTemplate() {
    return restTemplate;
  }
}
