package com.prash.packages.api;

import java.util.List;

public interface CurrencyConverterService {
  double convert(String baseCurrency, String targetCurrency, double amount);
  List<String> getSupportedCurrencies();
}
