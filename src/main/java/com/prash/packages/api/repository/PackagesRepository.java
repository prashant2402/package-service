package com.prash.packages.api.repository;

import com.prash.packages.model.PackageModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

public interface PackagesRepository extends MongoRepository<PackageModel, String> {}
