package com.prash.packages.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * A delegate to be called by the {@link CurrencyApiController}}. Implement this interface with a
 * {@link org.springframework.stereotype.Service} annotated class.
 */
@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.java.SpringCodegen",
    date = "2018-10-01T16:51:06.712+01:00[Europe/London]")
public interface CurrencyApiDelegate {

  Logger log = LoggerFactory.getLogger(CurrencyApi.class);

  default Optional<ObjectMapper> getObjectMapper() {
    return Optional.empty();
  }

  default Optional<HttpServletRequest> getRequest() {
    return Optional.empty();
  }

  default Optional<String> getAcceptHeader() {
    return getRequest().map(r -> r.getHeader("Accept"));
  }

  /** @see CurrencyApi#getCurrencies */
  default ResponseEntity<List<String>> getCurrencies() {
    if (getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {

    } else {
      log.warn(
          "ObjectMapper or HttpServletRequest not configured in default CurrencyApi interface so no example is generated");
    }
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }
}
