package com.prash.packages.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.java.SpringCodegen",
    date = "2018-09-28T21:13:52.846+01:00[Europe/London]")
@Controller
@CrossOrigin("http://localhost:4200")
public class PackagesApiController implements PackagesApi {

  private final PackagesApiDelegate delegate;

  @org.springframework.beans.factory.annotation.Autowired
  public PackagesApiController(PackagesApiDelegate delegate) {
    this.delegate = delegate;
  }

  @Override
  public PackagesApiDelegate getDelegate() {
    return delegate;
  }
}
