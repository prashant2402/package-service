package com.prash.packages.api;

import com.prash.packages.model.Product;
import java.util.List;

public interface ProductService {

  List<Product> listProducts();

  List<Product> getProducts(List<String> productIds);
}
