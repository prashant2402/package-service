package com.prash.packages.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prash.packages.model.PackageModel;
import com.prash.packages.model.PackageRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

/**
 * A delegate to be called by the {@link PackagesApiController}}. Implement this interface with a
 * {@link org.springframework.stereotype.Service} annotated class.
 */
@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.java.SpringCodegen",
    date = "2018-09-28T21:13:52.846+01:00[Europe/London]")
public interface PackagesApiDelegate {

  Logger log = LoggerFactory.getLogger(PackagesApi.class);

  default Optional<ObjectMapper> getObjectMapper() {
    return Optional.empty();
  }

  default Optional<HttpServletRequest> getRequest() {
    return Optional.empty();
  }

  default Optional<String> getAcceptHeader() {
    return getRequest().map(r -> r.getHeader("Accept"));
  }

  /** @see PackagesApi#createPackage */
  default ResponseEntity<?> createPackage(PackageRequest body) {
    if (getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {

    } else {
      log.warn(
          "ObjectMapper or HttpServletRequest not configured in default PackagesApi interface so no example is generated");
    }
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }

  /** @see PackagesApi#deletePackage */
  default ResponseEntity<?> deletePackage(String packageId) {
    if (getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {

    } else {
      log.warn(
          "ObjectMapper or HttpServletRequest not configured in default PackagesApi interface so no example is generated");
    }
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }

  /** @see PackagesApi#getPackage */
  default ResponseEntity<?> getPackage(String packageId, String currency) {
    if (getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {

    } else {
      log.warn(
          "ObjectMapper or HttpServletRequest not configured in default PackagesApi interface so no example is generated");
    }
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }

  /** @see PackagesApi#listPackages */
  default ResponseEntity<List<PackageModel>> listPackages() {
    if (getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {

    } else {
      log.warn(
          "ObjectMapper or HttpServletRequest not configured in default PackagesApi interface so no example is generated");
    }
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }

  /** @see PackagesApi#updatePackage */
  default ResponseEntity<?> updatePackage(PackageRequest body, String packageId) {
    if (getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {

    } else {
      log.warn(
          "ObjectMapper or HttpServletRequest not configured in default PackagesApi interface so no example is generated");
    }
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }
}
