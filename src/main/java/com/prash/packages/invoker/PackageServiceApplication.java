package com.prash.packages.invoker;

import com.prash.packages.api.ApiOriginFilter;
import com.prash.packages.exception.RestTemplateResponseErrorHandler;
import javax.servlet.Filter;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackages = {"com.prash.packages"})
@EnableMongoRepositories(basePackages = "com.prash.packages.api.repository")
public class PackageServiceApplication implements CommandLineRunner {

  public static void main(String[] args) throws Exception {
    new SpringApplication(PackageServiceApplication.class).run(args);
  }

  @Override
  public void run(String... arg0) throws Exception {
    if (arg0.length > 0 && arg0[0].equals("exitcode")) {
      throw new ExitException();
    }
  }

  @Bean
  public FilterRegistrationBean apiOriginFilterRegistration() {
    FilterRegistrationBean registration = new FilterRegistrationBean();
    registration.setFilter(apiOriginFilter());
    registration.addUrlPatterns("/*");
    registration.setName("apiOriginFilter");
    registration.setOrder(Integer.MAX_VALUE - 1);
    return registration;
  }

  @Bean(name = "apiOriginFilter")
  public Filter apiOriginFilter() {
    return new ApiOriginFilter();
  }

  class ExitException extends RuntimeException implements ExitCodeGenerator {
    private static final long serialVersionUID = 1L;

    @Override
    public int getExitCode() {
      return 10;
    }
  }
}
