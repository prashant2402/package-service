package com.prash.packages.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/** PackageModel */
@Validated
@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.java.SpringCodegen",
    date = "2018-09-28T21:13:52.846+01:00[Europe/London]")
@Document(collection = "packages")
public class PackageModel implements Serializable{

  @JsonProperty("id")
  @Id
  private String id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("products")
  @Valid
  private List<Product> products = new ArrayList<>();

  @JsonProperty("price")
  private Double price = null;

  public PackageModel id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   *
   * @return id
   */
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public PackageModel name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   *
   * @return name
   */
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public PackageModel description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Get description
   *
   * @return description
   */
  @ApiModelProperty(value = "")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public PackageModel products(List<Product> products) {
    this.products = products;
    return this;
  }

  public PackageModel addProductsItem(Product productsItem) {

    this.products.add(productsItem);
    return this;
  }

  /**
   * Get products
   *
   * @return products
   */
  @ApiModelProperty(required = true, value = "")
  @NotNull
  @Valid
  public List<Product> getProducts() {
    return products;
  }

  public void setProducts(List<Product> products) {
    this.products = products;
  }

  public PackageModel price(Double price) {
    this.price = price;
    return this;
  }

  /**
   * Get price
   *
   * @return price
   */
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PackageModel packageModel = (PackageModel) o;
    return Objects.equals(this.id, packageModel.id)
        && Objects.equals(this.name, packageModel.name)
        && Objects.equals(this.description, packageModel.description)
        && Objects.equals(this.products, packageModel.products)
        && Objects.equals(this.price, packageModel.price);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, description, products, price);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PackageModel {\n");

    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    products: ").append(toIndentedString(products)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }


  public Double calculateTotalPrice() {
    if (!products.isEmpty()) {
      Integer priceInCents = products.stream()
          .map(Product::getUsdPrice).mapToInt(value -> value).sum();
      Double totalPrice = priceInCents.doubleValue() / 100;
      return new BigDecimal(totalPrice.toString()).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }
    return null;
  }

  public Double calculatePriceForOtherCurrency(final double exchangeRate) {
    Double totalPrice = this.price * exchangeRate;
    return new BigDecimal(totalPrice.toString()).setScale(2, RoundingMode.HALF_UP).doubleValue();
  }
}
