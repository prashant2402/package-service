package com.prash.packages.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/** PackageRequest */
@Validated
@javax.annotation.Generated(
    value = "io.swagger.codegen.languages.java.SpringCodegen",
    date = "2018-09-28T21:13:52.846+01:00[Europe/London]")
public class PackageRequest {

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("productIds")
  @Valid
  private List<String> productIds = null;

  public PackageRequest name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   *
   * @return name
   */
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public PackageRequest description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Get description
   *
   * @return description
   */
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public PackageRequest productIds(List<String> productIds) {
    this.productIds = productIds;
    return this;
  }

  public PackageRequest addProductIdsItem(String productIdsItem) {

    if (this.productIds == null) {
      this.productIds = new ArrayList<>();
    }

    this.productIds.add(productIdsItem);
    return this;
  }

  /**
   * Get productIds
   *
   * @return productIds
   */
  @ApiModelProperty(value = "")
  public List<String> getProductIds() {
    return productIds;
  }

  public void setProductIds(List<String> productIds) {
    this.productIds = productIds;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PackageRequest packageRequest = (PackageRequest) o;
    return Objects.equals(this.name, packageRequest.name)
        && Objects.equals(this.description, packageRequest.description)
        && Objects.equals(this.productIds, packageRequest.productIds);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, description, productIds);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PackageRequest {\n");

    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    productIds: ").append(toIndentedString(productIds)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
