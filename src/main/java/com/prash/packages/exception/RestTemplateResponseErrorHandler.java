package com.prash.packages.exception;

import com.prash.packages.exception.ApiException;
import com.prash.packages.exception.BadRequestException;
import com.prash.packages.exception.NotFoundException;
import java.io.IOException;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

@Component
public class RestTemplateResponseErrorHandler
        implements ResponseErrorHandler {

    @Override
    public boolean hasError(ClientHttpResponse httpResponse)
            throws IOException {

        return (httpResponse
                .getStatusCode()
                .series() == HttpStatus.Series.CLIENT_ERROR || httpResponse
                .getStatusCode()
                .series() == HttpStatus.Series.SERVER_ERROR);
    }

    @Override
    public void handleError(ClientHttpResponse httpResponse)
            throws IOException {

        if (httpResponse
                .getStatusCode()
                .series() == HttpStatus.Series.SERVER_ERROR) {
            throw new ApiException(500, httpResponse.getStatusText());
        }
        if (httpResponse
                .getStatusCode()
                .series() == HttpStatus.Series.CLIENT_ERROR) {
            if (httpResponse.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new NotFoundException(404, httpResponse.getStatusText());
            }
            if (httpResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
                throw new BadRequestException(400, httpResponse.getStatusText());
            }
            throw new ApiException(httpResponse.getRawStatusCode(), httpResponse.getStatusText());
        }
    }
}