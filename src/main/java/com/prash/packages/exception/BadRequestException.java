package com.prash.packages.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-09-25T16:17:29.664+01:00")

@ResponseStatus(value= HttpStatus.BAD_REQUEST)
public class BadRequestException extends ApiException {
    private int code;

    public BadRequestException(int code, String msg) {
        super(code, msg);
        this.code = code;
    }
}
